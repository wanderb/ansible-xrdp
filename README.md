# Ansible-xrdp

This role configures an `xrdp` service on RHEL8 hosts (or compatible, like
CentOS 8 and recent Fedoras)

## Dependencies

This role requires the "Extra Packages for Enterprise Linux" (EPEL) repository
to be configured on RHEL8 and CentOS 8 nodes. Fedora nodes need nothing extra.
An easy way to configure EPEL is to use the `geerlingguy.repo-epel` role from
Ansible Galaxy.

## Variables

This role allows the configuration of the following variables:

- `xrdp_enable_xorg`
   Whether or not to configure an `xorgxrdp` backend session[^backend-sessions]

   Default: `true`
 - `xrdp_enable_xvnc
   Whether or not to configure an `Xvnc` backend session[^backend-sessions]

   Default: `true`
- `xrdp_default_session`
   The default (autorun) session for users who connect with username and
   password directly, without going through the login screen. If empty the
   first defined session in the configuration file will be used.

   Default: ""
- `xrdp_extra_config`
   Extra configuration to add to `xrdp.ini`
   Format: A list of dictionaries with `section`, `key`, and `value` keys.

   Default: `[]`
- `xrdp_install_desktop`
  The desktop environment to install. Valid choices: `gnome`, `kde`, and `xfce`.

  Default: `gnome`
- `xrdp_loglevel`
  The logging level for the `xrdp` service.

  Default: `INFO`
- `xrdp_enable_firewalld`
  Whether or not to attempt to open up RDP on the firewall.

  Default: `true`
- `xrdp_config_custom_logo_file_source`
  The filename on the control node (if any) that should be configured in the
  login screen.

  Default: ""
- `xrdp_config_custom_logo_filename`
  The filename on the configured node for the custom login screen logo.

  Default: `/usr/share/xrdp/logo.bmp`
- `xrdp_config_custom_logo`
  A list of dictionaries with `section`, `key`, and `value` keys for configuring the login screen

  Default:
  ```yaml
  - section: Globals
    key: ls_logo_filename
    value: "{{ xrdp_config_custom_logo_filename }}"
  - section: Globals
    key: blue
    value: e4002a
  - section: Globals
    key: ls_logo_x_pos
    value: 25
  - section: Globals
    key: ls_logo_y_pos
    value: 45
  - section: Globals
    key: ls_title
    value: "Login to {{ ansible_fqdn | default(inventory_hostname) }}"
  - section: Globals
    key: ls_top_window_bg_color
    value: 000000
  - section: Globals
    key: ls_bg_color
    value: ffffff
  ```

## Example playbook
```yaml
---
- name: Install and configure xrdp
  hosts:
  - all
  become: true
  gather_facts: true
  roles:
  - geerlingguy.repo-epel
  - ansible-xrdp
```

[^backend-sessions]: At least one of `xrdp_enable_xorg` and `xrdp_enable_xvnc`
  need to be set to `true` for a working installation.
